# -*- mode: ruby -*-
# vi: set ft=ruby :

if ENV.has_key?("KDE_ANSIBLE_PATH") then
  ansible_path = ENV["KDE_ANSIBLE_PATH"]
else
  ansible_path = "../kde-ansible"
end

Vagrant.configure("2") do |config|

  if Vagrant.has_plugin?("vagrant-cachier")
    config.cache.scope = :machine
  end

  config.vm.box = "ubuntu/xenial64"

  # webserver will be accessible via localhost:8080
  config.vm.network "forwarded_port", guest: 80, host: 8080
  # mailhog is at localhost:8025
  config.vm.network "forwarded_port", guest: 8025, host: 8025
  config.vm.network "forwarded_port", guest: 389, host: 50389

  config.vm.provider "virtualbox" do |vb|
    vb.linked_clone = true # faster VM creation
  end
  config.vm.provider "libvirt" do |virt, override|
    virt.volume_cache = "unsafe"
    override.vm.box = "nrclark/xenial64-minimal-libvirt"
    override.vm.synced_folder './', '/vagrant', type: 'rsync'
  end

  # Ansible needs Python already installed
  config.vm.provision "shell", inline: "sudo apt-get update && sudo apt-get install -y python python-apt"

  config.vm.provision "ansible" do |ansible|
    ansible.playbook = "playbook.yml"
    ansible.compatibility_mode = "2.0"
    ansible.become = true
    ansible.raw_arguments = ["--diff"]
    ansible.extra_vars = {"playbook_path": ansible_path}

    ansible.groups = {
      "apache" => ["default"],
      "identity" => ["default"],
      "all:vars" => {
        # this makes the playbook take the website code from the Vagrant
        # shared folder instead of cloning it from git
        'vagrant_local_code': true,

        # make ldap listen on public network interfaces instead of only localhost;
        # required for forwarding the LDAP port outside the VM.
        'ldap_listen_global': true,

        'enable_ssl': false,
        'identity_hostname': 'localhost:8080',
        'ldap_root_password_hash': "{SSHA}TWEXqh4aPmrbiXJAW6k7tLbvT7euDSzD",
        'ldap_root_password': 'testpassword',
        'ldap_solena_service_password': 'servicepw',
        'ldap_dev_env': true,
        'mysql_root_password': 'mysqlroot',
        'mysql_user_password': 'mysqlpassword',

        # these are test keys that will always let users through
        'recaptcha_sitekey': '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI',
        'recaptcha_secret': '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe'
      }
    }
  end

  config.trigger.before [:up, :provision] do |trigger|
    unless File.file?(File.join(ansible_path, "identity.yml")) then
      trigger.warn=<<-EOS
Error: Ansible playbooks not found.

A clone of sysadmin/kde-ansible is required in ../kde-ansible,
or set the KDE_ANSIBLE_PATH environment variable to override the path.
EOS
      trigger.abort=1
    end
  end

end
