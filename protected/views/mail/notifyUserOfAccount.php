<?php
	$mailer->subject = Yii::app()->name . ' account has been activated'
?>
Hello <?php echo $model->cn; ?>,

Thanks for activating your account on <?php echo Yii::app()->name ?>.

Details of your account are below.
Please keep this mail for your records.

Should you need any assistance regarding your account, please feel free to reply to this email.

Username: <?php echo $model->uid."\n"; ?>
Email:    <?php echo $model->mail."\n"; ?>

Thanks,
<?php echo Yii::app()->name; ?> site administrators.
